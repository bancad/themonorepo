import babel from "rollup-plugin-babel"
import nodeResolve from "rollup-plugin-node-resolve"
import commonjs from "rollup-plugin-commonjs"

import path from "path"

const { LERNA_PACKAGE_NAME } = process.env;
const PACKAGE_ROOT_PATH = process.cwd();
const PKG_JSON = require(path.join(PACKAGE_ROOT_PATH, "package.json"));
const INPUT_FILE = path.join(PACKAGE_ROOT_PATH, `${PKG_JSON.srcFile}/${PKG_JSON.srcName}.js`);
const OUTPUT_DIR = path.join(PACKAGE_ROOT_PATH, "dist");
const IS_BROWSER_BUNDLE = !!PKG_JSON.browser;

const LOCAL_GLOBALS = {
    'react': 'React',
    'react-dom': 'ReactDOM'
  };
  
const LOCAL_EXTERNALS = [
    'react',
    'react-dom'
  ];
  
  const formats = IS_BROWSER_BUNDLE ? ["umd"] : ["es", "cjs"];
  
  export default formats.map(format => ({
    plugins: [
      nodeResolve({
        "jsnext:main": true,
        "browser:main": true
      }),
      commonjs({
        include: /node_modules/,
        namedExports: {
          "react-js": ["isValidElementType"]
        }
      }),
      babel({
        exclude: ["node_modules/**"],
        presets: [['@babel/preset-env', {'modules': false}],'@babel/react'],
        plugins: [['@babel/plugin-proposal-class-properties', { 'loose': true }]]
      }),
    ],
    input: INPUT_FILE,
    
    external: [
      'ms',
      'react',
      'react-dom',
      'next',
      'next/link',
      'styled-jsx',
      id => /^react|styled-jsx/.test(id)
    ],
    output: {
      file: path.join(OUTPUT_DIR, `${PKG_JSON.srcName}.${format}.js`),
      format: 'umd', 
      sourcemap: true,
      name: LERNA_PACKAGE_NAME,
      amd: {
        id: LERNA_PACKAGE_NAME
      },
      globals: LOCAL_GLOBALS
    },
  }));