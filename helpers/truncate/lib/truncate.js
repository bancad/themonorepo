'use strict';

module.exports = truncate;

function truncate(text, maxchar) {
    let truncated = text

    if (truncated.length > maxchar) {
        truncated = truncated.substr(0,maxchar) + ' ... '
    }
    return truncated
}
