(function (factory) {
    typeof define === 'function' && define.amd ? define('@fdn/truncate', factory) :
    factory();
}((function () { 'use strict';

    module.exports = truncate;

    function truncate(text, maxchar) {
      var truncated = text;

      if (truncated.length > maxchar) {
        truncated = truncated.substr(0, maxchar) + ' ... ';
      }

      return truncated;
    }

})));
//# sourceMappingURL=truncate.umd.js.map
